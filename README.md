This README is intended to be helpful for colleagues at GitLab. For everything else, check https://nick.malcolm.net.nz/!

## Preferred engagement style

Please `@` me in comments, issues, and MRs. I use TODOs, and rarely check notification emails. You can also DM me on Slack.

My timezone affords me quiet Mondays and I like to keep these meeting free. 

## My role

I'm a Security Engineer in the Product Security Engineering team ([role description](https://handbook.gitlab.com/job-families/security/security-engineer/#product-security-engineering), [team page](https://handbook.gitlab.com/handbook/security/product-security/product-security-engineering/)). Our mission is to create proactive and preventative security controls which will scale with the organization and result in improved product security.

## Communication & working style

I tend to be concise and direct. Some might say blunt. If I overstep please [give me feedback](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/) so I can improve!

I like to prepare & be organised (I love lists!) vs. "winging it", but counter that with being loosely attached to my ideas/plans/processes and love to change things in response to what the situation needs. I have a strong bias for action.

### Strengths 

In August 2023 I took [the Strengths Finder assessment](https://en.wikipedia.org/wiki/Gallup_Test). It was interesting! A summary is below. Team members are more than welcome to read the full report - send a DM.

Top 5:

  1. Relator
  1. Strategic
  1. Maximizer
  1. Adaptability
  1. Learner

<details><summary>Click to expand more on this topic</summary>

The top 5 are below. The top 10 can change order by situation but - in general - won't drop out of the top 10. The bottom strengths are areas that don't come as naturally. All strengths, top or bottom, have potential weaknesses or blind spots.

> NOTE: This following content is from the report which is "Copyright © 2000, 2018 Gallup, Inc."

![A DNA-looking image showing my strengths rated from 1 to 34](https://gitlab.com/nmalcolm/nmalcolm/-/raw/main/images/strengths.png)

#### Relator

> You naturally form genuine and mutually rewarding one-on-one relationships. Your authenticity allows you to build close, long-lasting connections that foster trust and confidence.

- I enjoy close relationships with others
- I find satisfaction in working hard with friends to achieve a goal
- I need to be careful of my tendency to only spend time with people I already know

#### Strategic

> You quickly weigh alternative paths and determine the best one. Your natural ability to anticipate, play out different scenarios and plan ahead makes you an agile decision-maker.

- I attempt to understand how and why things work or fail to work
- I'm more innovative when I have ample time to process ideas
- Because I can do this quickly, it may look as if I am "winging it". I need to remember to explain myself
- I need to be mindful of what is already working well and what others have accomplished, so that my actions aren't interpreted as criticism

#### Maximizer

> You focus on quality, and you prefer working with and for the best. By seeing what each person naturally does best and empowering them to do it, you make individuals, teams and groups better.

- I focus on strengths as a way to stimulate personal and group excellence. I seek to transform something strong into something superb.
- I figure out what I do well and aim my efforts in that direction
- I deal with everyone the same way, and avoid special treatment
- I need to manage my own expectations "when a project or initiative falls short of your definition of excellence"

#### Adaptability

> You are flexible and can quickly adjust to changing or unexpected circumstances

- I prefer to go with the flow. (`Nick's note`: I like to plan _and then_ go with the flow!)
- I have an ability to savor the good things in life as they unfold
- I am flexible enough to handle unexpected discoveries and situations
- I need to be aware that frequent changes can feel endless and often unnecessary to those who thrive on structure and stability

#### Learner

> You love to learn, and you intuitively know how you learn best. Your natural ability to pick up and absorb information quickly and to challenge yourself to continually learn more keeps you on the cutting edge.

- I have a desire to learn and want to continuously improve. The process of learning, rather than the outcome, excites me
- I prefer to gradually expand my knowledge; I am not in a hurry to gain a new skill or acquire a new piece of information
- To grow I need to discipline myself to sign up for a course or class each year
- I need to be careful not to let the process of knowledge acquisition get in the way of my results and productivity.

</details>

## Māori Kupu

I try to use te reo Māori kupu in my messages, particularly in Slack. Here are some common ones:

- Kia ora (Hello)
- Tēnā koe/korua/koutou (Hello - to 1/2/many people)
- Ngā mihi (Thanks)
- e hoa / e hoa mā (friend(s))

[MaoriDictionary.co.nz](https://maoridictionary.co.nz/) can be helpful too!

## Personal

I am married and have two boys ❤️ They might interrupt a meeting or mean I need to postpone. Thanks for understanding!

I like (in no particular order):

- Trail running
- Family time
- Dungeons & Dragons
- Lasagne
- Going to church
- Blobbing in front of the TV
- Reading fantasy (Riftwar Cycle, Wheel of Time, Kingkiller Chronicle, Mistborn, etc) or popcorn thriller (Jack Reacher)
- Learning about New Zealand history & te ao Māori

### Work books

Books that have influenced my work are:

- **[Creativity, Inc by Ed Catmul](https://en.wikipedia.org/wiki/Creativity,_Inc.)**. Security is all about creativity IMO. The bullet point recap at the end is great. I re-read it once a year-ish.
- **[HBR's 10 Must Reads - The Essentials](https://store.hbr.org/product/hbr-s-10-must-reads-the-essentials/13292)**. I like how each article has "Idea in Brief" and "in Practice" sections to make skim reading & refreshing easier.
- **[Agile Application Security by O'Reilly](https://learning.oreilly.com/library/view/agile-application-security/9781491938836/)**. A good reference book especially when teaching concepts to newer AppSec folks
- **[Shouting Zeros and Ones](https://www.bwb.co.nz/books/shouting-zeros-and-ones/)**. NZ-specific on technology, society, harm, etc.

## Home office setup

- 2021 16" M1 Max, 32GB RAM
- **Displays**
  - Dell P3421W ultra-wide display
  - Loctek Pro Mount for the screen
  - NEXTSTAND for the laptop, placed to the right
- **Desk** Evolve Electric Standing Desk (1200x700 English Oak)
- **Chair** Loctek YZ101 Ergonomic Office chair (would not recommend)
- **Inputs**
  - Logitech Ergo K860 Ergonomic Bluetooth Keyboard
  - Contour Unimouse, left-handed
  - Yubikey
- **Audio / Visual**
  - Logitech C920 Webcam
  - Rode NT-USB Mini, on a cheap boom arm
  - Bose QuietComfort 35 II Wireless Over-ear Headset
  - Vibes Hi-Fidelity Earplugs (not used for work, but I like 'em)
- **Lighting**
  - Logitech Litra Glow Light
  - South and West facing windows (I'm in the southern hemisphere)
  - Using the MBP's "Studio Light" + "Portrait" mode makes this set up look great
- **Guitars** because no home office is complete without pretentious wall mounted guitars ;)
  - 2006 Cole Clark FL1AC. Old faithful. Solid Bunya, Queensland maple, made in Australia.
  - Tanglewood Winterleaf 2. Cheap and cheerful 3/4 size, for kapa haka at kid's school.
  - ESP LTD B-4E Bass. Solid.
  - Lanikai Ukulele. Fun.
  - Squire Bullet Strat. Another cheap & cheerful.

