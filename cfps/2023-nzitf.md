### Context

https://nzitf.org.nz/call-for-papers

> Te Papa Museum, Wellington (& online) on the 13-14th November 2023

> close to two hundred senior InfoSec professionals. It provides an opportunity for attendees to learn from other industry professionals, exchange ideas, and network with peers in a high-trust environment

> if you have a subject you are passionate about, then let us know and we’ll review and consider it. We have presentation slots of 15, 30 and 45 mins

> We recommend you use a presentation title that is descriptive rather than humorous. ... If you are selected, we will need to see your slides before you present.


### Submission

Submit your presentation abstract by emailing speakers@nzitf.org.nz and include:

- **`A Short Title`** 
  - ~~Living life in the TLP:Clear~~ (humorous, not descriptive)
  - ⭐️ How transparency empowers security, builds trust, and inspires collaboration
  - How security & transparency can co-exist with great results
  - Lessons on security from "the most transparent company in the word"
  - `Something else...`
- **`Abstract (approx 100-400 words outlining what you wish to talk about)`**

What would it be like to work in a place where security is powered in large part by publicly available documentation and runbooks? Where almost every security issue - including PoC, response, and code - is made public? Many attendees might work in organisations where the ability to be transparent about security ranges from difficult to impossible. For a small number of orgs that's the way it has to be and will always be - but is there another way for the rest of us? 

Nick Malcolm works in security at GitLab, "the most transparent company in the world". In this presentation attendees will learn how transparency and security work together, its advantages and challenges, and walk away with ideas for how aspects of this approach might be applied within their own organisations.

Key topics will include how GitLab's security team lives the public-by-default company value, how transparency differs by content and audience (security, team members, public), the benefits it gives those audiences, and how transparency enables and inspires collaboration.

This talk is vendor agnostic; it is not about GitLab the product. It is about what Nick has observed at GitLab _the company_, and how that might be applied more widely in other organisations.

---

Nick specialises in Application Security and is an AppSec Engineer at GitLab, having previously been a consultant in roles like Cloud Security & Governance. He regularly presents at meetups and conferences, including CHCon 2022 & 2020, OWASP Day 2020 and 2017, and CyberCon AU 2018.

- **`Duration (15 mins, 30 mins, 45 mins)`** 30 min (could go 45 if you like: more examples!)
- **`TLP Level (TLP:RED, TLP:AMBER+STRICT, TLP:AMBER, TLP:GREEN, or TLP:CLEAR)`** TLP:CLEAR
- **`Location (In-person or Remotely presented over Zoom)`** In-person
- **`Do you require flight reimbursement? (Y/N)`** No
- **`Do you require accommodation reimbursement? (Y/N)`** No

### Things to cover

- The transparency value. This whole thing is supported by executive buy-in, and potential hires are evaluated on their ability to live this value.
- Public by default, with exceptions - how Security lives that value.
- The handbook. Public processes allow for understanding and trust, and invite collaboration.
  - Transparency only works if the stuff you're making transparent is trustworthy / accurate / etc
- Public security issues. (Again) public processes allow for understanding and trust from security researchers. 
- Automation helps re-enforce the value
  - The transparency reminder bot
  - Bots that enforce what our public processes say we'll do
- Things we have going for us:
  - We're open core & GitLab has been doing transparency since Day 1
  - We've got a legal team to support "public company" stuff (as in public on the stock exchange public) and our SAFE framework
  - Single Source(s) of Truth - we don't have lots of different systems to manage transparency over. Handbook for processes. GitLab Issues for issues. YouTube for meetings.
- Challenges:
  - Being a public company: regulations & legal considerations (reference our SAFE framework). 
  - The urge / tendency to default to non-public
  - Not making (summaries of) SIRT issues or Root Cause Analysis public is an example of where we could improve (IMO), but that's a tricky problem to solve
  - (Trivial) low-value bug bounty reports / info disclosure reports; vastly outweighed by the quality reports though.

### References

- https://handbook.gitlab.com/handbook/values/#transparency
- https://about.gitlab.com/handbook/security/transparency_by_default.html
- https://about.gitlab.com/blog/2022/02/17/how-gitlab-handles-security-bugs/
- https://about.gitlab.com/blog/2020/04/28/benefits-of-transparency-in-compliance/
- https://about.gitlab.com/blog/2019/09/05/the-difference-transparency-makes-in-security/
- https://gitlab.com/nmalcolm/nmalcolm/-/issues/1+
